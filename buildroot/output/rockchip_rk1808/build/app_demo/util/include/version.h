/*
 * Copyright 2010 Rockchip Electronics S.LSI Co. LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __VERSION_H__
#define __VERSION_H__

#define	CODE_VERSION    "d931cd410c047bea5738adb6fb36f48d7e2182c4"
#define	CODE_AUTHOR     "Lin Huang"
#define	CODE_DATE       ""
#define	CODE_ONE_LINE   "d931cd4 author: Lin Huang image_process: isp camera also use mmap"
#define	CODE_VER_NUM    "-1"

#endif /*__VERSION_H__*/

/* vi: set noexpandtab syntax=c: */

