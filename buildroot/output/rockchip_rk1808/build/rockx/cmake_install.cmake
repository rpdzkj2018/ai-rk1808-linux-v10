# Install script for directory: /home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "TRUE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/rockx" TYPE DIRECTORY FILES "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/include/")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/lib64/librknn_api.so"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/lib64/librockx.so"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/carplate_align.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/carplate_detection.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/carplate_recognition.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_attribute.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_detection.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_landmark5.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_landmarks68.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_liveness_2d.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/face_recognition.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/head_detection.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/object_detection.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/pose_body.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/pose_finger.data"
    "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/sdk/rockx-rk1808-Linux/../rockx-data/pose_hand.data"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/rpdzkj_debug/first/tanzh/rk1808-linux-v10/0303/rk1808_linux_v1.0_20200303/buildroot/output/rockchip_rk1808/build/rockx/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
